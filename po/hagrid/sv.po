#
# Translators:
# Felicia Jongleur, 2021
#
msgid ""
msgstr ""
"Project-Id-Version: hagrid\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-15 16:33-0700\n"
"PO-Revision-Date: 2019-09-27 18:05+0000\n"
"Last-Translator: Felicia Jongleur, 2021\n"
"Language-Team: Swedish (https://www.transifex.com/otf/teams/102430/sv/)\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

msgid "Error"
msgstr "Fel"

msgid "Looks like something went wrong :("
msgstr "Det verkar som att något gick snett :("

msgid "Error message: {{ internal_error }}"
msgstr "Felmeddelande: {{ internal_error }}"

msgid "There was an error with your request:"
msgstr "Det uppstod ett fel med din begäran:"

msgid "We found an entry for <span class=\"email\">{{ query }}</span>:"
msgstr "Vi hittade en post för <span class=\"email\">{{ query }}</span>:"

msgid ""
"<strong>Hint:</strong> It's more convenient to use <span class=\"brand"
"\">keys.openpgp.org</span> from your OpenPGP software.<br /> Take a look at "
"our <a href=\"/about/usage\">usage guide</a> for details."
msgstr ""
"<strong>Tips:</strong> Det är enklare att använda <span class=\"brand\">keys."
"openpgp.org</span> från din OpenPGP-mjukvara.<br /> Kolla in vår <a href=\"/"
"about/usage\">användarguide</a> för mer detaljer."

msgid "debug info"
msgstr "felsökningsinfo"

msgid "Search by Email Address / Key ID / Fingerprint"
msgstr "Sök efter e-postadress / nyckel-ID / fingeravtryck"

msgid "Search"
msgstr "Sök"

msgid ""
"You can also <a href=\"/upload\">upload</a> or <a href=\"/manage\">manage</"
"a> your key."
msgstr ""
"Du kan också <a href=\"/upload\">ladda upp</a> eller <a href=\"/manage"
"\">hantera</a> din nyckel."

msgid "Find out more <a href=\"/about\">about this service</a>."
msgstr "Läs mer  <a href=\"/about\">om den här tjänsten</a>."

msgid "News:"
msgstr "Nyheter:"

msgid ""
"<a href=\"/about/news#2019-11-12-celebrating-100k\">Celebrating 100.000 "
"verified addresses! 📈</a> (2019-11-12)"
msgstr ""
"<a href=\"/about/news#2019-11-12-celebrating-100k\">Vi firar 100 000 "
"verifierade adresser! 📈</a> (2019-11-12)"

msgid "v{{ version }} built from"
msgstr "v{{ version }} byggd från"

msgid "Powered by <a href=\"https://sequoia-pgp.org\">Sequoia-PGP</a>"
msgstr "Drivs av <a href=\"https://sequoia-pgp.org\">Sequoia-PGP</a>"

msgid ""
"Background image retrieved from <a href=\"https://www.toptal.com/designers/"
"subtlepatterns/subtle-grey/\">Subtle Patterns</a> under CC BY-SA 3.0"
msgstr ""
"Bakgrundsbild hämtad från <a href=\"https://www.toptal.com/designers/"
"subtlepatterns/subtle-grey/\">Subtle Patterns</a> under CC BY-SA 3.0"

msgid "Maintenance Mode"
msgstr "Underhållsläge"

msgid "Manage your key"
msgstr "Hantera din nyckel"

msgid "Enter any verified email address for your key"
msgstr "Ange någon verifierad e-postadress för din nyckel"

msgid "Send link"
msgstr "Skicka länk"

msgid ""
"We will send you an email with a link you can use to remove any of your "
"email addresses from search."
msgstr ""
"Vi kommer att skicka ett e-postmeddelande till dig med en länk som du kan "
"använda för att ta bort någon av dina e-postadresser från sökresultaten."

msgid ""
"Managing the key <span class=\"fingerprint\"><a href=\"{{ key_link }}\" "
"target=\"_blank\">{{ key_fpr }}</a></span>."
msgstr ""
"Hanterar nyckeln <span class=\"fingerprint\"><a href=\"{{ key_link }}\" "
"target=\"_blank\">{{ key_fpr }}</a></span>."

msgid "Your key is published with the following identity information:"
msgstr "Din nyckel är publicerad med följande identitetsinformation:"

msgid "Delete"
msgstr "Radera"

msgid ""
"Clicking \"delete\" on any address will remove it from this key. It will no "
"longer appear in a search.<br /> To add another address, <a href=\"/upload"
"\">upload</a> the key again."
msgstr ""
"Att klicka \"radera\" på en adress kommer att ta bort den från den här "
"nyckeln. Den kommer inte längre att visas i en sökning.<br /> För att lägga "
"till en annan adress, <a href=\"/upload\">ladda upp</a>nyckeln igen. "

msgid ""
"Your key is published as only non-identity information.  (<a href=\"/about\" "
"target=\"_blank\">What does this mean?</a>)"
msgstr ""
"Din nyckel är nu publicerad som endast icke-identifierbar information.  (<a "
"href=\"/about\" target=\"_blank\">Vad betyder detta?</a>)"

msgid "To add an address, <a href=\"/upload\">upload</a> the key again."
msgstr ""
"För att lägga till en adress, <a href=\"/upload\">ladda upp</a> nyckeln igen."

msgid ""
"We have sent an email with further instructions to <span class=\"email"
"\">{{ address }}</span>."
msgstr ""
"Vi har skickat ett e-postmeddelande med vidare instruktioner till <span "
"class=\"email\">{{ address }}</span>."

msgid "This address has already been verified."
msgstr "Den här adressen har redan verifierats."

msgid ""
"Your key <span class=\"fingerprint\">{{ key_fpr }}</span> is now published "
"for the identity <a href=\"{{userid_link}}\" target=\"_blank\"><span class="
"\"email\">{{ userid }}</span></a>."
msgstr ""
"Din nyckel <span class=\"fingerprint\">{{ key_fpr }}</span> är nu publicerad "
"för identiteten <a href=\"{{userid_link}}\" target=\"_blank\"><span class="
"\"email\">{{ userid }}</span></a>."

msgid "Upload your key"
msgstr "Ladda upp din nyckel"

msgid "Upload"
msgstr "Ladda upp"

msgid ""
"Need more info? Check our <a target=\"_blank\" href=\"/about\">intro</a> and "
"<a target=\"_blank\" href=\"/about/usage\">usage guide</a>."
msgstr ""
"Behöver du mer information? Kolla in vår <a target=\"_blank\" href=\"/about"
"\">introduktion</a> och <a target=\"_blank\" href=\"/about/usage"
"\">användarguide</a>."

msgid ""
"You uploaded the key <span class=\"fingerprint\"><a href=\"{{ key_link }}\" "
"target=\"_blank\">{{ key_fpr }}</a></span>."
msgstr ""
"Du laddade upp nyckeln <span class=\"fingerprint\"><a href="
"\"{{ key_link }}\" target=\"_blank\">{{ key_fpr }}</a></span>."

msgid "This key is revoked."
msgstr "Den här nyckeln är återkallad."

msgid ""
"It is published without identity information and can't be made available for "
"search by email address (<a href=\"/about\" target=\"_blank\">what does this "
"mean?</a>)."
msgstr ""
"Den är nu publicerad utan identitetsinformation och kan inte göras "
"tillgänglig för sökning via e-postadress (<a href=\"/about\" target=\"_blank"
"\">vad betyder detta?</a>)."

msgid ""
"This key is now published with the following identity information (<a href="
"\"/about\" target=\"_blank\">what does this mean?</a>):"
msgstr ""
"Den här nyckeln är nu publicerad med följande identitetsinformation (<a href="
"\"/about\" target=\"_blank\">vad betyder detta?</a>):"

msgid "Published"
msgstr "Publicerad"

msgid ""
"This key is now published with only non-identity information. (<a href=\"/"
"about\" target=\"_blank\">What does this mean?</a>)"
msgstr ""
"Den här nyckeln är nu publicerad med endast icke-identifierbar information. "
"(<a href=\"/about\" target=\"_blank\">Vad betyder detta?</a>)"

msgid ""
"To make the key available for search by email address, you can verify it "
"belongs to you:"
msgstr ""
"Du kan verifiera att nyckeln tillhör dig för att göra den sökbar via e-"
"postadress:"

msgid "Verification Pending"
msgstr "Avvaktar verifiering"

msgid ""
"<strong>Note:</strong> Some providers delay emails for up to 15 minutes to "
"prevent spam. Please be patient."
msgstr ""
"<strong>Obs:</strong> Vissa leverantörer fördröjer e-postmeddelanden i upp "
"till 15 minuter för att förhindra spam. Ha tålamod."

msgid "Send Verification Email"
msgstr "Skicka verifieringsmeddelande"

msgid ""
"This key contains one identity that could not be parsed as an email address."
"<br /> This identity can't be published on <span class=\"brand\">keys."
"openpgp.org</span>.  (<a href=\"/about/faq#non-email-uids\" target=\"_blank"
"\">Why?</a>)"
msgstr ""
"Den här nyckeln innehåller en identitet som inte kunde behandlas som en e-"
"postadress.<br /> Den här identiteten kan inte publiceras på <span class="
"\"brand\">keys.openpgp.org</span>.  (<a href=\"/about/faq#non-email-uids\" "
"target=\"_blank\">Varför?</a>)"

msgid ""
"This key contains {{ count_unparsed }} identities that could not be parsed "
"as an email address.<br /> These identities can't be published on <span "
"class=\"brand\">keys.openpgp.org</span>.  (<a href=\"/about/faq#non-email-"
"uids\" target=\"_blank\">Why?</a>)"
msgstr ""
"Den här nyckeln innehåller {{ count_unparsed }}  identiteter som inte kunde "
"behandlas som en e-postadress.<br /> Dessa identiteter kan inte publiceras "
"på <span class=\"brand\">keys.openpgp.org</span>.  (<a href=\"/about/faq#non-"
"email-uids\" target=\"_blank\">Varför?</a>)"

msgid ""
"This key contains one revoked identity, which is not published. (<a href=\"/"
"about/faq#revoked-uids\" target=\"_blank\">Why?</a>)"
msgstr ""
"Den här nyckeln innehåller en återkallad identitet, som inte är publicerad. "
"(<a href=\"/about/faq#revoked-uids\" target=\"_blank\">Varför?</a>)"

msgid ""
"This key contains {{ count_revoked }} revoked identities, which are not "
"published. (<a href=\"/about/faq#revoked-uids\" target=\"_blank\">Why?</a>)"
msgstr ""
"Den här nyckeln innehåller {{ count_revoked }} återkallade identiteter, som "
"inte är publicerade. (<a href=\"/about/faq#revoked-uids\" target=\"_blank"
"\">Varför?</a>)"

msgid "Your keys have been successfully uploaded:"
msgstr "Din nycklar har laddats upp:"

msgid ""
"<strong>Note:</strong> To make keys searchable by email address, you must "
"upload them individually."
msgstr ""
"<strong>Obs:</strong> För att göra nycklar sökbara via e-postadress behöver "
"du ladda upp dem en och en."

msgid "Verifying your email address…"
msgstr "Verifierar din e-postadress…"

msgid ""
"If the process doesn't complete after a few seconds, please <input type="
"\"submit\" class=\"textbutton\" value=\"click here\" />."
msgstr ""
"Om processen inte slutförs efter några sekunder, vänligen <input type="
"\"submit\" class=\"textbutton\" value=\"click here\" />."

msgid "Manage your key on {{domain}}"
msgstr "Hantera din nyckel på {{domain}}"

msgid "Hi,"
msgstr "Hej,"

msgid ""
"This is an automated message from <a href=\"{{base_uri}}\" style=\"text-"
"decoration:none; color: #333\">{{domain}}</a>."
msgstr ""
"Detta är ett automatiskt meddelande från <a href=\"{{base_uri}}\" style="
"\"text-decoration:none; color: #333\">{{domain}}</a>."

msgid "If you didn't request this message, please ignore it."
msgstr "Om du inte bad om det här meddelandet, vänligen ignorera det."

msgid "OpenPGP key: <tt>{{primary_fp}}</tt>"
msgstr "OpenPGP-nyckel: <tt>{{primary_fp}}</tt>"

msgid ""
"To manage and delete listed addresses on this key, please follow the link "
"below:"
msgstr ""
"För att hantera och radera listade adresser på den här nyckeln, följ länken "
"nedan:"

msgid ""
"You can find more info at <a href=\"{{base_uri}}/about\">{{domain}}/about</"
"a>."
msgstr ""
"Du kan hitta mer information på <a href=\"{{base_uri}}/about\">{{domain}}/"
"about</a>."

msgid "distributing OpenPGP keys since 2019"
msgstr "distribuerar OpenPGP-nycklar sedan 2019"

msgid "This is an automated message from {{domain}}."
msgstr "Detta är ett automatiskt meddelande från {{domain}}."

msgid "OpenPGP key: {{primary_fp}}"
msgstr "OpenPGP-nyckel: {{primary_fp}}"

msgid "You can find more info at {{base_uri}}/about"
msgstr "Du kan hitta mer information på {{base_uri}}/about"

msgid "Verify {{userid}} for your key on {{domain}}"
msgstr "Verifiera {{userid}} för din nyckel på {{domain}}"

msgid ""
"To let others find this key from your email address \"<a rel=\"nofollow\" "
"href=\"#\" style=\"text-decoration:none; color: #333\">{{userid}}</a>\", "
"please click the link below:"
msgstr ""
"För att låta andra hitta den här nyckeln från din e-postadress \"<a rel="
"\"nofollow\" href=\"#\" style=\"text-decoration:none; color: "
"#333\">{{userid}}</a>\", klicka på länken nedan:"

msgid ""
"To let others find this key from your email address \"{{userid}}\",\n"
"please follow the link below:"
msgstr ""
"För att låta andra hitta den här nyckeln från din e-postadress "
"\"{{userid}}\",\n"
"klicka på länken nedan:"

msgid "No key found for fingerprint {}"
msgstr "Ingen nyckel hittades för fingeravtrycket {}"

msgid "No key found for key id {}"
msgstr "Ingen nyckel hittades för nyckel-ID {}"

msgid "No key found for email address {}"
msgstr "Ingen nyckel hittades för e-postadressen {}"

msgid "Search by Short Key ID is not supported."
msgstr "Att söka efter kort nyckel-ID stöds ej."

msgid "Invalid search query."
msgstr "Ogiltig sökterm."

msgctxt "Subject for verification email, {0} = userid, {1} = keyserver domain"
msgid "Verify {0} for your key on {1}"
msgstr "Verifiera {0} för din nyckel på {1}"

msgctxt "Subject for manage email, {} = keyserver domain"
msgid "Manage your key on {}"
msgstr "Hantera din nyckel på {}"

msgid "This link is invalid or expired"
msgstr "Den här länken är ogiltig eller har upphört att gälla"

#, fuzzy
msgid "Malformed address: {}"
msgstr "Felformaterad adress: {address}"

#, fuzzy
msgid "No key for address: {}"
msgstr "Ingen nyckel för adress: {address}"

msgid "A request has already been sent for this address recently."
msgstr "En begäran har redan skickats för den här adressen nyligen."

msgid "Parsing of key data failed."
msgstr "Behandling av nyckelinformation misslyckades."

msgid "Whoops, please don't upload secret keys!"
msgstr "Hoppsan, ladda inte upp privata nycklar tack!"

msgid "No key uploaded."
msgstr "Ingen nyckel laddades upp."

msgid "Error processing uploaded key."
msgstr "Fel när uppladdad nyckel bearbetades."

msgid "Upload session expired. Please try again."
msgstr "Uppladdningssession gick ut. Vänligen försök igen."

msgid "Invalid verification link."
msgstr "Ogiltig verifieringslänk."
